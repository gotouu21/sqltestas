CREATE SCHEMA `testas`;

USE testas;



drop table LeidinioAutoriai;
drop table Zurnalas;
drop table Atsiliepimai;
drop table Naudotojai;
drop table Autoriai;
drop table Leidiniai;
drop table Statusai;
drop table Zanrai;

CREATE TABLE Zanrai (
	ZanroID int primary key auto_increment,
    Pavadinimas varchar(200)
);

insert into Zanrai (ZanroID, Pavadinimas) values
('1','Novele'),
('2','Komedija');

CREATE TABLE Statusai (
	StatusoID int primary key auto_increment,
    Pavadinimas varchar(200)
);

insert into Statusai (StatusoID, Pavadinimas) values
('1','Laisva'),
('2','Uzimta');

CREATE TABLE Leidiniai (
	LeidinioID int primary key auto_increment,
    ZanroID int,
    StatusoID int,
    LeidimoData int,
    foreign key (ZanroID) references Zanrai (ZanroID),
    foreign key (StatusoID) references Statusai (StatusoID)
);

CREATE TABLE Autoriai (
	AutoriausID int primary key auto_increment,
    Vardas varchar(20)
);

CREATE TABLE LeidinioAutoriai (
	LeidinioID int,
    AutoriausID int,
	foreign key (LeidinioID) references Leidiniai (LeidinioID),
    foreign key (AutoriausID) references Autoriai (AutoriausID)
);

insert into Leidiniai (LeidimoData, ZanroID, StatusoID) values
('2011','1','1'),
('2000','2','1'),
('2014','2','2'),
('2013','1','2'),
('2010','2','1'),
('2018','2','1');

CREATE TABLE Naudotojai (
	NaudotojoID int primary key auto_increment,
    Vardas varchar(20)
);

CREATE TABLE Zurnalas (
	ZurnaloID int primary key auto_increment,
    NaudotojoID int,
    LeidinioID int,
    foreign key (NaudotojoID) references Naudotojai (NaudotojoID),
    foreign key (LeidinioID) references Leidiniai (LeidinioID),
    PriemimoData date,
    GrazinimoData date
);

CREATE TABLE Atsiliepimai (
	AtsiliepimoID int primary key auto_increment,
    Tekstas varchar(255),
    Reitingas int,
    Data datetime,
    NaudotojoID int,
    foreign key (NaudotojoID) references Naudotojai (NaudotojoID) 
);

insert into Autoriai(AutoriausID, Vardas) values
('1','Martynas'),
('2','Donatas'),
('3','Denis'),
('4','Michael');

insert into Naudotojai(NaudotojoID, Vardas) values
('1','Nikolas'),
('2','Aleksas'),
('3','Matas'),
('4','Dominyka'),
('5','Jurij'),
('6','Acbads');

insert into Atsiliepimai (Reitingas) values
('5'),
('2.5'),
('3.6'),
('4'),
('1');

insert into Zurnalas (PriemimoData, GrazinimoData) values
('2018-10-11','2018-10-22'),
('2013-10-11','2015-10-12');

/* UZDUOTYS */

/* 1 */

select * from Leidiniai
where LeidimoData < 2016;

/* 2 */

select * from Atsiliepimai
where Reitingas > 3.3;

/* 3 */

select * from Naudotojai
where Vardas Like 'a%_b%s';

/* 4 */

select * from Leidiniai
Where StatusoID = 1;

/* 5 */

select * from LeidinioAutoriai;

/* 6 */

select * from Zurnalas
where 
